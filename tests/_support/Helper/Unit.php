<?php
/**
 * HomeSelectorImporter plugin for Craft CMS 3.x
 *
 * Imports Home Selector entries and assets
 *
 * @link      https://www.prettyabstract.co.uk/
 * @copyright Copyright (c) 2020 Andrew Wallace
 */

namespace Helper;

use Codeception\Module;

/**
 * Class Unit
 *
 * Here you can define custom actions.
 * All public methods declared in helper class will be available in $I
 *
 */
class Unit extends Module
{

}
