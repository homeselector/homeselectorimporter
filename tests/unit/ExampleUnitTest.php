<?php
/**
 * HomeSelectorImporter plugin for Craft CMS 3.x
 *
 * Imports Home Selector entries and assets
 *
 * @link      https://www.prettyabstract.co.uk/
 * @copyright Copyright (c) 2020 Andrew Wallace
 */

namespace pixelimage\homeselectorimportertests\unit;

use Codeception\Test\Unit;
use UnitTester;
use Craft;
use pixelimage\homeselectorimporter\HomeSelectorImporter;

/**
 * ExampleUnitTest
 *
 *
 * @author    Andrew Wallace
 * @package   HomeSelectorImporter
 * @since     1.0.0
 */
class ExampleUnitTest extends Unit
{
    // Properties
    // =========================================================================

    /**
     * @var UnitTester
     */
    protected $tester;

    // Public methods
    // =========================================================================

    // Tests
    // =========================================================================

    /**
     *
     */
    public function testPluginInstance()
    {
        $this->assertInstanceOf(
            HomeSelectorImporter::class,
            HomeSelectorImporter::$plugin
        );
    }

    /**
     *
     */
    public function testCraftEdition()
    {
        Craft::$app->setEdition(Craft::Pro);

        $this->assertSame(
            Craft::Pro,
            Craft::$app->getEdition()
        );
    }
}
