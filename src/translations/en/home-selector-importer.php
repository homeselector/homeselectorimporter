<?php
/**
 * HomeSelectorImporter plugin for Craft CMS 3.x
 *
 * Imports Home Selector entries and assets
 *
 * @link      https://www.prettyabstract.co.uk/
 * @copyright Copyright (c) 2020 Andrew Wallace
 */

/**
 * HomeSelectorImporter en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('home-selector-importer', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Andrew Wallace
 * @package   HomeSelectorImporter
 * @since     1.0.0
 */
return [
    'HomeSelectorImporter plugin loaded' => 'HomeSelectorImporter plugin loaded',
];
