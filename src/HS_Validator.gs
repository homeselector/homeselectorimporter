// @version 1.2.8
//
// Don't look at this, Michael.
//
// Sets up formulae to extract data from HomeSelector, used for validation and population of data columns. 
//
// Once populated, this workbook to be exported to .xlsx for importing using the Homeselector Importer Craft plugin.

function onOpen(){
  var ss_plots = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("plots");
  var ss_remote = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("remote data");
  
  var clientDataPoints = [
    {"range":"M5","group":"developments"},
    {"range":"A5","group":"homeStyles"},
    {"range":"U5","group":"garage-options"},
    {"range":"X5","group":"extension-options"},
    {"range":"U20","group":"outside-options"}
  ];
  var siteDataPoints = [
    {"range":"I5","group":"phases"},
    {"range":"Q5","group":"buildings"}
  ];
  
  setImportFormulae(ss_remote, clientDataPoints);
  
  // remote data source ranges
  var r_phaseNames = ss_remote.getRange('I5:I30');
  var r_homeTypeNames = ss_remote.getRange('A5:A100');  
  var r_buildingNames = ss_remote.getRange('Q5:Q30');
  var r_siteHandleNames = ss_remote.getRange('N5:N20');
  var r_garageNames = ss_remote.getRange('V5:V15');
  var r_extNames = ss_remote.getRange('Y5:Y15');
  var r_outsideNames = ss_remote.getRange('V20:V30');
  
  // pixel input ranges
  var pixel_siteHandle = ss_remote.getRange('B2');
  
  // user input ranges
  var user_phaseNames = ss_plots.getRange('B4:B300');
  var user_homeTypes = ss_plots.getRange('H4:H300');
  var user_buildings = ss_plots.getRange('E4:E300');
  var user_siteHandle = ss_plots.getRange('B1');
  var user_exts = ss_plots.getRange('L4:L300');
  var user_outside = ss_plots.getRange('M4:M300');
  var user_garages = ss_plots.getRange('N4:N300');
  
  // bin off any existing validation rules
  ss_plots.getDataRange().clearDataValidations();
  
  refreshImport(ss_remote);
  
  // phases
  setUserEntryValidationRules(r_phaseNames, user_phaseNames, "Select a valid Phase");
  // home types
  setUserEntryValidationRules(r_homeTypeNames, user_homeTypes, "Select a valid Home Style");
  // buildings
  setUserEntryValidationRules(r_buildingNames, user_buildings, "Select a valid Building");
  // garages
  setUserEntryValidationRules(r_garageNames, user_garages, "Select a valid garage option");
  // extensions
  setUserEntryValidationRules(r_extNames, user_exts, "Select a valid extension option");  // extensions
  // outside space
  setUserEntryValidationRules(r_outsideNames, user_outside, "Select a valid outdoor space option");

  
  // set available site handles
  setUserEntryValidationRules(r_siteHandleNames, pixel_siteHandle, "Select a valid Development");
  
  // set the site handle to the first one retrieved
  if (r_siteHandleNames.getValues().length) {
    console.log("Setting site handle");
    pixel_siteHandle.setValue( r_siteHandleNames.getValues()[0][0] );
    user_siteHandle.setFormula("='remote data'!B2"); 
    
    setImportFormulae(ss_remote, siteDataPoints);
  } else {
    console.log("No site handles");
  }
  
  var ui = SpreadsheetApp.getUi();
  ui.createMenu("Homeselector")
    .addItem("Refresh data", "refreshData")
    .addToUi();
}

function onEdit(e){
  var ss_remote = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("remote data");
  var ss_plots = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("plots");
  

  var activeCell = ss_plots.getActiveCell();

  var r_phases = ss_remote.getRange('I5:K30');
  var r_homeTypes = ss_remote.getRange('A5:C100');
  var r_buildings = ss_remote.getRange('Q5:S30');
  var r_garages = ss_remote.getRange('U5:V15');
  var r_exts = ss_remote.getRange('X5:Y15');
  var r_outside = ss_remote.getRange('U20:V30');
  
  var siteDataPoints = [
    {"range":"I5","group":"phases"},
    {"range":"Q5","group":"buildings"}
  ];

  if(activeCell.getColumn() == 2 && activeCell.getRow() < 3){  
    // should wait for the clientDPs to finish before doing this:
    // cachebust
    refreshImport(ss_remote);
    setImportFormulae(ss_remote, siteDataPoints);
  }
  
 
  // development selector
  if(activeCell.getColumn() == 2 && activeCell.getRow() == 1){
    // change the phases and buildings query    
    refreshImport(ss_remote);
    setImportFormulae(ss_remote, siteDataPoints);
  }
  
  // populate phase data from selection
  if(activeCell.getColumn() == 2 && activeCell.getRow() >=4){
    populateAutoData(activeCell, r_phases, [1,2]);
  }
  
  // buildings
  if(activeCell.getColumn() == 5 && activeCell.getRow() >=4){
    populateAutoData(activeCell, r_buildings, [1,2]);
  }    
  
  // home types
  if(activeCell.getColumn() == 8 && activeCell.getRow() >=4){
    populateAutoData(activeCell, r_homeTypes, [1,2]);
  }  
  

/*
  // extensions
  if(activeCell.getColumn() == 17 && activeCell.getRow() >=4){
    populateAutoData(activeCell, r_garages, [1]);
  }

  // garages
  if(activeCell.getColumn() == 19 && activeCell.getRow() >=4){
    populateAutoData(activeCell, r_exts, [1]);
  }
*/

}

function refreshData() {
  onOpen();
  return true; 
}

function populateAutoData(activeCell, range, sources){
    if (! activeCell.isBlank()) {
      var lu_values = range.getValues();
      var lu_rowIdx = rowIndexForSearchString(activeCell.getValue(), range, 0);
    
      if (lu_rowIdx >= 0) {
        sources.forEach(function(idx){
          activeCell.offset(0,idx).setValue(lu_values[lu_rowIdx][idx]);
        });        
      } else {
        Logger.log("Cannot match " + activeCell.getValue() + " in range");
      }
    } else {
      sources.forEach(function(idx){
        activeCell.offset(0,idx).clearContent();
      });
    }    
}

function setUserEntryValidationRules(validRange, userRange, helpMessage) {
  var rule = SpreadsheetApp
    .newDataValidation()
    .requireValueInRange(validRange)
    .setAllowInvalid(false)
    .setHelpText(helpMessage)
    .build();
  
  // apply to cells in the column
  userRange.setDataValidation(rule);
}


// return the row index for the found value
function rowIndexForSearchString(lookup, range, colIdx) {
  var values = range.getValues();
  for (var row=0;row<values.length;row++){
    if (values[row][colIdx] == '') {
      continue;
    } else if (values[row][colIdx] == lookup){
      return row;
    }
  }
}

function getSelectedSite(remoteData) {
  var siteHandle = remoteData.getRange("B2").getValue();
  return siteHandle;
}

function refreshImport(remoteData){
  var lock = LockService.getScriptLock();
  if (! lock.tryLock(30000)) {
    console.log("Not refreshing import URL");
    return;
  }
  console.log("Refreshing import URL");
  var now = new Date().getTime();
  remoteData.getRange("E1").setValue( now.toString() );
  lock.releaseLock();  
}

function getServerUrl(remoteData) {
  return remoteData.getRange("B1").getValue();
}

function makeBaseUrl(serverName, component) {
  if (! component.length ) {
    component = 'all';
  }
  return serverName + '/actions/home-selector-module/remote-data/' + component;
}

function setImportFormulae(remoteData, datapoints){
  var serverName = getServerUrl(remoteData); // e.g https://homeselector.com
  
  if (serverName) {
    var baseUrl = makeBaseUrl(serverName, 'all');
    var chosenSite = getSelectedSite(remoteData);
    var cacheKey = remoteData.getRange("E1").getValue();
    datapoints.forEach(function(datapoint){
      if (datapoint == 'developments') {
        chosenSite = '';
      }
      remoteData.getRange(datapoint.range).setFormula('=IMPORTXML("' + baseUrl + '?' + cacheKey + '&site=' + chosenSite + '","//' + datapoint.group + '//item")'); 
    });
  }
}