<?php
/**
 * HomeSelectorImporter plugin for Craft CMS 3.x
 *
 * Imports Home Selector entries and assets
 *
 * @link      https://www.prettyabstract.co.uk/
 * @copyright Copyright (c) 2020 Andrew Wallace
 */

namespace pixelimage\homeselectorimporter\models;

use pixelimage\homeselectorimporter\HomeSelectorImporter;

use Craft;
use craft\base\Model;

/**
 * Settings Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Andrew Wallace
 * @package   HomeSelectorImporter
 * @since     1.0.0
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * Some model attribute
     *
     * @var string
     */

    public $autoAssetsConfig = '{}';
    public $useAutoAssets = false;

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['autoAssetsConfig', 'string'],
            ['useAutoAssets', 'boolean']
        ];
    }
}
