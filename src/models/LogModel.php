<?php

namespace pixelimage\homeselectorimporter\models;

use craft\base\Model;
use Craft;

/**
 * Class LogModel
 *
 * @property-read ElementInterface|Element|null $element
 */
class LogModel extends Model
{
    // Properties
    // =========================================================================

    public $id;
    public $siteId;
    public $importType;
    public $message;
    public $dateCreated;
    public $dateUpdated;
    public $uid;

    // Model-only properties
    public $debug;
    public $paginationUrl;


    // Public Methods
    // =========================================================================

    public function __toString()
    {
        return Craft::t('home-selector-importer', $this->importType);
    }

}
