<?php

namespace pixelimage\homeselectorimporter\models;

use craft\base\Model;
use Craft;

/**
 * Class ImportModel
 *
 * @property-read ElementInterface|Element|null $element
 */
class ImportModel extends Model
{
    // Properties
    // =========================================================================

    public $id;
    public $userId;
    public $siteId = '1';
    public $importType = '';
    public $uploadedData = [];
    public $dataStatus = null;
    public $dateCreated;
    public $dateUpdated;
    public $uid;

    // Model-only properties
    public $debug;
    public $paginationUrl;


    // Public Methods
    // =========================================================================

    public function __toString()
    {
        return Craft::t('home-selector-importer', $this->importType);
    }

}
