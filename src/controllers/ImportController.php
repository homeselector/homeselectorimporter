<?php
/**
 * HomeSelectorImporter plugin for Craft CMS 3.x
 *
 * Imports Home Selector entries and assets
 *
 * @link      https://www.prettyabstract.co.uk/
 * @copyright Copyright (c) 2020 Andrew Wallace
 */

namespace pixelimage\homeselectorimporter\controllers;

use Carbon\Carbon;
use pixelimage\homeselectorimporter\HomeSelectorImporter;
use pixelimage\homeselectorimporter\records\ImportRecord;
use pixelimage\homeselectorimporter\models\ImportModel;

use Craft;
use craft\web\Controller;
use Craft\helpers\Json;

/**
 * Import Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Andrew Wallace
 * @package   HomeSelectorImporter
 * @since     1.0.0
 */
class ImportController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    
    // protected $allowAnonymous = ['index', 'do-something'];

    // Public Methods
    // =========================================================================


    public function actionDownloadPlots()
    {
        // Return a template .xlsx file with all the good stuff in it for uploading plots.

    }

    /**
     * Process the uploaded file
     * 
     */
    public function actionUploadPlots()
    {
        $this->requirePermission('importPlots');

        Craft::$app->getSession()->set('homeselector', null);
        $settings = HomeSelectorImporter::$plugin->getSettings();
        $plugin = HomeSelectorImporter::getInstance();
        $store = Craft::getAlias('@storage')."/hsimporter/"; 
        $now = Carbon::now()->toDateTimeString();

        $import = new ImportModel();

        // bin off anything that's in here already
        ImportRecord::deleteAll();

        $import->siteId = null;
        $import->importType = '';
        $import->uploadedData = [];
        $import->dataStatus = null;
        $import->userId = Craft::$app->getUser()->getIdentity()->getId();        
        
        if( isset( $_FILES['file'] ) ) {
            
            $file_name = $_FILES['file']['name'];
            $file_size = $_FILES['file']['size'];
            $file_tmp  = $_FILES['file']['tmp_name'];
            $file_type = $_FILES['file']['type'];

            move_uploaded_file($file_tmp, $store . "/" .$file_name);


            $ext       = pathinfo($file_name, PATHINFO_EXTENSION);
            if( $ext == 'xlsx' ) {

                
                $preflightData = $plugin->importPlots->importPlotsData($store . "/" .$file_name);
                if ( count($preflightData['errors']) > 0 ) {
                    Craft::$app->getSession()->addFlash(
                        'homeselector', [
                            "errors" => $preflightData['errors'], 
                            "message" => "Pre-flight import failed"
                        ]);
                    // return to the import tab
                    return $this->redirect( 'home-selector-importer');                    
                } else {

                    // Upload looks ok, save and go to data verification                                       
                    
                    $import->dataStatus = 'preflight';
                    $import->uploadedData = Json::encode($preflightData);
                    
                    $record = new ImportRecord();
                    $record->dataStatus = $import->dataStatus;
                    $record->uploadedData = $import->uploadedData;
                    $record->userId = $import->userId;
                    $record->save(false);
                    
                    return $this->redirect('home-selector-importer/verify');
                }
            } else {
                Craft::$app->session->setError('Upload a HomeSelector .xlsx import file');
            }                              
        } else {
            Craft::$app->session->setError('Upload a HomeSelector .xlsx import file');
        }


    }

    /**
     * By this point, there should be something reasonably valid to import
     * in settings->uploadedData
     * 
     */
    public function actionImportPlots()
    {
        $this->requirePermission('importPlots');

        $plugin = HomeSelectorImporter::getInstance();
        $settings = HomeSelectorImporter::$plugin->getSettings();  
        $importer = HomeSelectorImporter::$plugin->importPlots;
        $errors = [];
        $imported = [];

        Craft::info("Attempt an import of uploaded, validated data", __METHOD__);
        $import = ImportRecord::find()->where(['userId' => Craft::$app->getUser()->getId()])->orderBy('id DESC')->one();

        if ($import->dataStatus == 'preflight valid') {

            try {
                // $actualData = Json::decode($settings->uploadedData);
                

                $actualData = Json::decode($import->uploadedData);
                if ( ! $importer->import($actualData, false) ) {
                    $errors = $importer->getImportErrors();
                } else {
                    $imported = $importer->getImportedData();
                }


                if ($errors && count($imported) == 0) {
                    // panic at the disco
                    Craft::$app->session->addFlash(
                        'homeselector', [
                            "errors" => $errors, 
                            "message" => "Import failed"
                        ]);

                    return $this->redirectToPostedUrl();
                                     

                } else {
                    if ($errors) {
                        $message = "Import partially successful";
                    } else {
                        $message = "Import successful";
                    }

                    Craft::$app->session->addFlash(
                        'homeselector', [
                            "errors" => $errors, 
                            "message" => $message . ". Imported " . count($imported) . " plots."
                    ]);

                    // Burn the data held in settings
                    // Craft::$app->getPlugins()->savePluginSettings( $plugin, [
                    //     'uploadedData' => null,
                    //     'dataStatus' => 'processed'
                    // ]); 

                    $import->uploadedData = null;
                    $import->dataStatus = 'processed';
                    $import->save(false);
                    
                    Craft::info("Redirecting to redirect " . Craft::$app->getRequest()->getValidatedBodyParam('redirect'), __METHOD__);
                    return $this->redirectToPostedUrl();

                }
            } catch (\Throwable $e) {
                Craft::$app->session->addFlash(
                    'homeselector', [
                        "errors" => $errors, 
                        "message" => "Import failed with exception: " . $e->getMessage()
                    ]);
        
            }
             
        } else {
            // shouldn't be here, the data hasn't been verified
            Craft::$app->session->addFlash(
                'homeselector', [
                    "errors" => [], 
                    "message" => "Data not verified yet"
                ]);
        }

    }

}
