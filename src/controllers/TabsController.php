<?php
/**
 * HomeSelectorImporter plugin for Craft CMS 3.x
 *
 * Imports Home Selector entries and assets
 *
 * @link      https://www.prettyabstract.co.uk/
 * @copyright Copyright (c) 2020 Andrew Wallace
 */

namespace pixelimage\homeselectorimporter\controllers;

use pixelimage\homeselectorimporter\HomeSelectorImporter;

use Craft;
use craft\web\Controller;
use craft\helpers\Json;
use pixelimage\homeselectorimporter\records\ImportRecord;

/**
 * Tabs Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Andrew Wallace
 * @package   HomeSelectorImporter
 * @since     1.0.0
 */
class TabsController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */

     // protected $allowAnonymous = ['import', 'settings', 'elementmap'];



    // Public Methods
    // =========================================================================

    public function actionImport()
    {
        $this->requirePermission('importPlots');
        $settings = HomeSelectorImporter::$plugin->getSettings();
        $this->renderTemplate( 'home-selector-importer/tabs/import', array(
            'setting' => $settings,
            'uploadedData' => null
        ));
    }

    /**
     * Data has been uploaded and passed some basic structural checks. Now,
     * do some more detailed verifcation and parse for pre-import presentation
     * to the user.
     * 
     */
    public function actionVerify()
    {
        $this->requirePermission('importPlots');
        $settings = HomeSelectorImporter::$plugin->getSettings();
        $importer = HomeSelectorImporter::$plugin->importPlots;

        $import = ImportRecord::find()->where(['userId' => Craft::$app->getUser()->getId()])->orderBy('id DESC')->one();

        $errors = null;

        if ($import) {
        
            if ($import->uploadedData) {
                // pre-flight the data
                $actualData = Json::decode($import->uploadedData);
                if ( ! $importer->import($actualData, true) ) {
                    $errors = $importer->getImportErrors();
                }
            } else {
                $errors[] = "No data has been uploaded";
            }

            if ($errors) {
                Craft::$app->getSession()->addFlash('homeselector', [
                    "errors" => $errors, 
                    "message" => "Pre-flight verification of the data failed."
                ]);
                $import->dataStatus = 'preflight invalid';
                $import->uploadedData = '{}';
                $import->save(false);
                // Craft::$app->getPlugins()->savePluginSettings( HomeSelectorImporter::$plugin, $settings->toArray() ); 
                return $this->redirect( 'home-selector-importer');
            } else {
                $import->dataStatus = 'preflight valid';
                $import->siteId = $importer->getSite()->id;
                $import->save(false);
                // Craft::$app->getPlugins()->savePluginSettings( HomeSelectorImporter::$plugin, $settings->toArray() ); 

                $preflightData = [
                    'header' => $importer->getHeader(),
                    'data' => $importer->getData(),
                    'site' => $importer->getSite(),
                    'plots' => $importer->getPlotsSection()
                ];
                $this->renderTemplate( 'home-selector-importer/tabs/verify', array(
                    'preflightData' => $preflightData
                ));
            }
        } else {
            $errors[] = "No data has been uploaded";
            Craft::$app->getSession()->addFlash('homeselector', [
                "errors" => $errors, 
                "message" => "Pre-flight verification of the data failed."
            ]);
            return $this->redirect( 'home-selector-importer');
        }

    }

    public function actionResult()
    {
        $this->requirePermission('importPlots');
        $settings = HomeSelectorImporter::$plugin->getSettings();
        $this->renderTemplate( 'home-selector-importer/tabs/result', array(
            'setting' => $settings
        ));
    }    
}
