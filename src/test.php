<?php
require_once('../vendor/autoload.php');
use OpenSpout\Reader\Common\Creator\ReaderFactory;

$reader = ReaderFactory::createFromFile('plots.xlsm');

$reader->open('plots.xlsm');

$data = extractData($reader);

$reader->close();

var_dump($data);

function extractData($reader) {
  $data = [
    "handles" => [],
    "data" => []
  ];

  $importSheet = "plots";
  $developmentDataRow = 0; // site handle
  $fieldHandlesRow = 1; // Craft field handles
  $dataHeader = 2; // user friendly names
  $dataFirstRow = 3; // where the plot data starts

  foreach ($reader->getSheetIterator() as $sheet) {
    if ($sheet->getName() == $importSheet) {
      $sheetRowCount = 0;
      $dataRowCount = 0;
      foreach ($sheet->getRowIterator() as $row) {
        $cells = $row->getCells();

        if ($sheetRowCount == $developmentDataRow) {
          // get the site handle
          echo "For site: " . $cells[1] . "\n";
        } elseif ($sheetRowCount == $fieldHandlesRow) {
          // get the Craft handles
          foreach ($cells as $cell) {
            $data['handles'][] = $cell->getValue();
          }
        } elseif ($sheetRowCount >= $dataFirstRow) {
          // the rest (data)
          $datarow = [];
          foreach ($cells as $cell) {
            $datarow[] = $cell->getValue();
          }
          $data['data'][] = $datarow;
          $dataRowCount++;
        }
        $sheetRowCount++;
      }
    }
  }
  return $data;
}
