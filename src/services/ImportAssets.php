<?php
/**
 * HomeSelectorImporter plugin for Craft CMS 3.x
 *
 * Imports Home Selector entries and assets
 *
 * @link      https://www.prettyabstract.co.uk/
 * @copyright Copyright (c) 2020 Andrew Wallace
 */

namespace pixelimage\homeselectorimporter\services;

use pixelimage\homeselectorimporter\HomeSelectorImporter;
use Cake\Utility\Hash;
use Craft;
use craft\base\Component;
use craft\elements\Asset;
use craft\elements\Entry;
use craft\helpers\Json;
use craft\models\FolderCriteria;
use Throwable;
use yii\base\InvalidArgumentException;


/**
 * ImportAssets Service
 *
 * PixelImage would like to pre-upload assets and have them magically appear in entries.
 * This bunch of logic attempts to do this, on a rather limited basis.
 *
 * When an entry is first created, any asset fields which we know about (detailed in $_config)
 * first have their dynamic paths resolved, then *any* files in here are added to the
 * Crafts asset index, so that they can then be attached to the Entry.
 *
 * @var $_config = [
 *       'plot' => ['pl_image'],
 *        'homeStyle' => ['ht_images', 'ht_plans3d', 'ht_plansColour', 'ht_plansLine'],
 *        'phase' => [
 *            'ph_aerialViews',
 *            'ph_selectorViews' => 'phaseView.viewImage'
 *      ]
 *    ];
 *
 * @author    Andrew Wallace
 * @package   HomeSelectorImporter
 * @since     1.0.0
 */
class ImportAssets extends Component
{

    /* JSON is something like this:
{
	"plot": ["pl_image"],
	"homeStyle": {
		"0": "ht_images",
		"ht_plansLine": "planLine.viewImageAsset",
		"ht_plansColour": "planColour.viewImageAsset",
		"ht_plans3d": "plan3D.viewImageAsset"
	},
	"phase": {
		"ph_selectorViews": "phaseView.viewImageAsset",
		"ph_topdownViews": "topdownView.viewImageAsset"
	},
	"building": {
		"bu_selectorViews": "buildingView.viewImageAsset",
		"bu_floorLevelMaps": "floorLevelMap.viewImageAsset"
	}
}

    */
    protected $_config = [
        'plot' => ['pl_image'],
        'homeStyle' => [
            'ht_images',
            'ht_plansLine' => 'planLine.viewImageAsset',
            'ht_plansColour' => 'planColour.viewImageAsset',
            'ht_plans3d' => 'plan3D.viewImageAsset'
        ],
        'phase' => [
            'ph_selectorViews' => 'phaseView.viewImageAsset',
            'ph_topdownViews' => 'topdownView.viewImageAsset'
        ],
        'building' => [
            'bu_selectorViews' => 'buildingView.viewImageAsset',
            'bu_floorLevelMaps' => 'floorLevelMap.viewImageAsset'
        ]
    ];

    /** @var Entry */
    protected $_entry;

    public function initAttach(Entry $entry)
    {
        $this->getConfig();

        $this->_entry = $entry;
        if ( $this->hasAutoAssets() ) {
            $this->indexAssetsForNewEntry();
        }
    }

    /**
     * Once the entry has been saved, loop over the asset fields,
     * with handle names we know about.
     */
    public function indexAssetsForNewEntry()
    {
        try {
            $assetFieldHandles = $this->_config[$this->_entry->type->handle];
            $newdata = [];

            // Craft::info("Ancestors: " . $this->_entry->ancestors->level(1)->one()->title, __METHOD__);
            Craft::info("Looking for a match in " . count($assetFieldHandles) . " handles with " . $this->_entry->type->handle, __METHOD__);

            // get the asset fields from our mini-config
            foreach ($assetFieldHandles as $key => $value) {
                // get the Craft field with this handle
                $isMatrixField = false;
                $fieldHandle = '';
                if (!is_int($key)) {
                    // matrix field, probably. dig it out.
                    $fielddef = explode('.',$assetFieldHandles[$key]);
                    $matrixBlockTypeHandle = $fielddef[0];
                    $matrixBlockFieldHandle = $fielddef[1];
                    Craft::info("I think " .$key." is a matrix field with block handle " . $matrixBlockTypeHandle . " and field " . $matrixBlockFieldHandle, __METHOD__);
                    $isMatrixField = true;
                    $fieldHandle = $key;
                    $matrixField = Craft::$app->getFields()->getFieldByHandle($fieldHandle);
                    $matrixBlockType = $this->getMatrixBlockTypeForHandle($matrixField, $matrixBlockTypeHandle);
                    $matrixBlockFields = $matrixField->getBlockTypeFields();
                    $assetField = Hash::extract($matrixBlockFields, '{n}[handle=' . $matrixBlockFieldHandle . ']')[0];

                } else {
                    $isMatrixField = false;
                    $fieldHandle = $value;
                    $assetField = Craft::$app->getFields()->getFieldByHandle($fieldHandle);
                }



                if ($assetField && $assetField->className() == 'craft\fields\Assets') {
                    Craft::info("Processing subfolder path: " . $assetField->singleUploadLocationSubpath, __METHOD__);
                    // get the real path name the entry will store the asset to
                    // this actually creates the folder if it doesn't exist too.
                    $folderId = $assetField->resolveDynamicPathToFolderId($this->_entry);
                    $folder = Craft::$app->assets->getFolderById($folderId);
                    $volume = $folder->volume;

                    Craft::info("Found asset field for handle " . $fieldHandle . ", folderId: " . $folderId, __METHOD__);

                    // get a list of files in the dynamic path location
                    $files = $volume->getFileList($folder->path, false);

                    $assetsIdsToAdd = $this->indexExistingFiles($files, $volume);

                    // if there are assets found, attach them to the entry
                    if ($assetsIdsToAdd) {
                        if ($isMatrixField) {

                            $blocks = [];
                            $sortOrder = (clone $this->_entry->$fieldHandle)->anyStatus()->ids();
                            $idx=1;
                            foreach ($assetsIdsToAdd as $assetId) {

                                $sortOrder[] = 'new:'.$idx;
                                $block = [
                                    'type' => $matrixBlockTypeHandle,
                                    'enabled' => true,
                                    'collapsed' => false,
                                    'fields' => [
                                       $matrixBlockFieldHandle => [$assetId]
                                    ]
                                ];
                                $blocks['new:'.$idx] = $block;

                                $idx++;
                            }
                            Craft::info("Setting field ". $fieldHandle, __METHOD__);
                            $newdata = [
                                'sortOrder' => $sortOrder,
                                'blocks' => $blocks
                            ];
                            $this->_entry->setFieldValue($fieldHandle, $newdata);
                        } else {
                            $newdata[$fieldHandle] = $assetsIdsToAdd;
                            Craft::info("Setting field ". $fieldHandle, __METHOD__);
                            $this->_entry->setFieldValue($fieldHandle, $assetsIdsToAdd);
                        }
                    } else {
                        Craft::info("No assets indexed to add to entry at handle " . $fieldHandle, __METHOD__);
                    }
                } else {
                    // No field with that handle, bork
                    if (!$assetField) {
                        Craft::error('Invalid field handle for entryType ' . $this->_entry->type->handle . ' found in config: ' . $fieldHandle, __METHOD__ );
                    } else {
                        Craft::error('Field with handle of ' . $fieldHandle . ' for entryType ' . $this->_entry->type->handle . ' found in config has element type of ' . $assetField->className(), __METHOD__ );
                    }
                }

            }

            // print_r($newdata);
            // exit;
            // if there's stuff to add, add it now
            if ($newdata) {
                $this->attachAssetsToNewEntry();
            }
        } catch (\Throwable $e) {
            Craft::error($e, __METHOD__);
        }
    }

    /**
     * Attaches the freshly indexed assets to the given handle on the entry
     */
    public function attachAssetsToNewEntry()
    {
        try {
            Craft::info("Saving assets to entry " . $this->_entry->title . ", id: ". $this->_entry->id . ", uid: ". $this->_entry->uid, __METHOD__);
            // don't set field values here, doing it with setFieldValue in each loop
            // $this->_entry->setFieldValues($data);
            // Craft::dump($this->_entry);
            // exit();

            $success = Craft::$app->elements->saveElement($this->_entry);
            Craft::info("Saved entry with auto-attached assets, id: ". $this->_entry->id . ", uid: ". $this->_entry->uid, __METHOD__);
            // print_r($data);
            // var_dump($this->_entry);
            // exit;

            if (!$success) {
                Craft::error('Couldn\'t add assets to entry',__METHOD__);
            }
            return $success;
        } catch (\Throwable $e) {
            Craft::error('Couldn\'t add assets to entry',__METHOD__);
            Craft::error($e->getMessage(), __METHOD__);
        }
    }

    /**
     * Add the files at the given path to the volume's asset index.
     * Returns an array of asset IDs for saving to the entry.
     *
     * @return mixed
     */
    public function indexExistingFiles($files, $volume)
    {
        $assetsToAdd = [];
        foreach ($files as $path => $file) {
            // just index it. should probably check to see if it
            // exists in the index, but no matter.
            Craft::info('Indexing ' . $file['path'] . ' in volume ' . $volume->id, __METHOD__);
            $asset = Craft::$app->assetIndexer->indexFile(
                $volume,
                $file['path']
            );
            $assetsToAdd[] = $asset->id;
        }
        return $assetsToAdd;
    }

    /**
     * Initial check to see if this entry has an entryType with
     * assets that we want to automatically attach.
     *
     * @return boolean
     */
    private function hasAutoAssets()
    {
        if ($this->_entry) {
            // entry type handle
            $handle = $this->_entry->type->handle;
            if ( array_key_exists($handle, $this->_config) ) {
                return true;
            } else {
                Craft::info("Nothing of interest here for entry type: " . $handle, __METHOD__);
                return false;
            }
        } else {
            // no Entry
            Craft::info("No entry to check for assets", __METHOD__);
            return false;
        }
    }

    public function setConfig($value)
    {
        $this->_config = $value;
        return $this;
    }

    public function validateConfig()
    {

    }

    public function getConfig()
    {
        try {
            // get any JSON from settings
            $configJSON = HomeSelectorImporter::getInstance()->settings->autoAssetsConfig;
            $config = Json::decode($configJSON);
            if ($config && count($config) > 0) {
                Craft::info("Found JSON in config with " . count($config) . ' entry types.', __METHOD__);
                $this->_config = $config;
            } else {
                // use the default
                Craft::info("No JSON found in config, using class default", __METHOD__);
            }
        } catch (InvalidArgumentException $e) {
            Craft::error("Invalid JSON in config", __METHOD__);
        }

    }

    private function getMatrixAssetField($field, $matrixField)
    {



    }

    private function getMatrixBlockTypeForHandle($field, $handle) {
        $blockTypes = $field->getBlockTypes();
        if ($blockTypes) {
            foreach ($blockTypes as $bt) {
                if ($bt->handle == $handle) {
                    return $bt;
                }
            }
        }
        // if we've got this far, error
        Craft::error("No matrix block type of ". $handle . " in field ".$field->handle, __METHOD__);
    }
}
