<?php
/**
 * HomeSelectorImporter plugin for Craft CMS 3.x
 *
 * Imports Home Selector entries and assets
 *
 * @link      https://www.prettyabstract.co.uk/
 * @copyright Copyright (c) 2020 Andrew Wallace
 */

namespace pixelimage\homeselectorimporter\services;

use pixelimage\homeselectorimporter\HomeSelectorImporter;

use Craft;
use craft\base\Component;
use craft\elements\Entry;
use craft\models\FolderCriteria;
use Craft\helpers\Json;
use OpenSpout\Reader\Common\Creator\ReaderFactory;
use OpenSpout\Reader\ReaderInterface;
use Carbon\Carbon;
use verbb\supertable\SuperTable;
use Exception;
use Throwable;

/**
 * ImportPlots Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Andrew Wallace
 * @package   HomeSelectorImporter
 * @since     1.0.0
 */
class ImportPlots extends Component
{
    private $_siteHandle;
    private $_plotEntryTypeHandle = 'plot';
    private $_plotsSectionHandle = 'development';

    /** @var \craft\models\Site */
    private $_site;
    /** @var \craft\models\Section */
    private $_plotsSection;
    private $_sectionEntryTypes = [];
    private $_header = [];
    private $_data = [];

    private $_author;

    // Stuff that can be in the import, but not in a fieldset
    private $_customSchema = [
        'title', 'building', 'phase', 'homeStyle-slug', 'phase-slug', 'building-slug'
    ];

    /**
     * required: is the field required for the import
     * type: string, list, entryId
     * entryType: the Craft entrytype if the 'type' is entryId
     * fv: is it a field value in the Plot entrytype fieldset
     * 
     */
    private $_entryTypeFields = [
        'title' => ['required' => true, 'type' => 'string', 'fv' => false],
        'pl_reverseSymmetry' => ['required' => false, 'type' => 'string', 'fv' => true],
        'pl_aptFloorLevel' => ['required' => false, 'type' => 'integer', 'fv' => true],
        'pl_extensions' => ['required' => false, 'type' => 'string', 'fv' => true],
        'pl_garage' => ['required' => false, 'type' => 'string', 'fv' => true],
        'pl_outsideSpace' => ['required' => false, 'type' => 'string', 'fv' => true],
        'ht_subtitle' => ['required' => false, 'type' => 'string', 'fv' => true],
        'ht_shortDescription' => ['required' => false, 'type' => 'string', 'fv' => true],
        'ht_longDescription' => ['required' => false, 'type' => 'string', 'fv' => true],
        'phase' => ['required' => true, 'type' => 'entryId', 'entryType' => 'phase', 'fv' => false],
        'building' => ['required' => false, 'type' => 'entryId', 'entryType' => 'building', 'fv' => false],
        'homeStyle' => ['required' => true, 'type' => 'entryId', 'entryType' => 'homeStyle', 'fv' => true],
        'pl_maskRef' => ['required' => false, 'type' => 'string', 'fv' => true]        
    ];

    protected $_importErrors = [];
    protected $_importMessage = null;

    protected $_importedData = [];
    protected $_failedImportData = [];



    /**
     * Reads from the file at the path/name given and parses contents.
     * 
     * @return mixed Structured array of data containing plots
     */
    public function importPlotsData($source)
    {
        $reader = ReaderFactory::createFromFile($source);
        $reader->open($source);
        $parsedData = $this->parsePlotsData($reader);
        $reader->close();
        return $parsedData;
    }


    /**
     * Do some basic validation on the uploaded file 
     * to ensure it's in the correct format. 
     * This doesn't set any class variables, so passing data via return.
     * 
     * @param ReaderInterface $reader A box/spout reader for the uploaded file
     */
    private function parsePlotsData(ReaderInterface $reader)
    {
        $result = [
            "site" => null,
            "handles" => [],
            "data" => [],
            "errors" => []
        ];

        $importSheet = "plots";
        $developmentDataRow = 0; // site handle
        $fieldHandlesRow = 1; // Craft field handles
        $dataHeader = 2; // user friendly names
        $dataFirstRow = 3; // where the plot data starts

        $colsWithHandles = [];

        $plotsSheetFound = false;

        foreach ($reader->getSheetIterator() as $sheet) {
            if ($sheet->getName() == $importSheet) {
                $plotsSheetFound = true;
                $sheetRowCount = 0;
                $dataRowCount = 0;
                foreach ($sheet->getRowIterator() as $row) {
                    $cells = $row->getCells();

                    if ($sheetRowCount == $developmentDataRow) {
                        // get the site handle
                        if (isset($cells[1])) {
                            $result['site'] = $cells[1]->getValue();
                        }
                    } elseif ($sheetRowCount == $fieldHandlesRow) {
                        // get the Craft handles
                        $col = 0;
                        foreach ($cells as $cell) {
                            if (! $cell->isEmpty() ) {
                                $result['handles'][] = $cell->getValue();
                                $colsWithHandles[$col] = $cell->getValue();
                            }
                            $col++;
                        }
                    } elseif ($sheetRowCount >= $dataFirstRow) {
                        // the rest (data)
                        $datarow = [];
                        $datacol = 0;
                        foreach ($cells as $idx => $cell) {
                            if ( array_key_exists($datacol, $colsWithHandles) ) {
                                $datarow[$colsWithHandles[$datacol]] = $cell->getValue();
                            }
                            $datacol++;
                        }
                        $result['data'][] = $datarow;
                        $dataRowCount++;
                    }
                    $sheetRowCount++;
                }
            }
        }

        if (!$plotsSheetFound) {
            $result['errors'][] = "No worksheet with the name plots found";
        }

        if (is_null($result['site'])) {
            $result['errors'][] = "Site name not found in A2";
        }

        if (empty($result['handles'])) {
            $result['errors'][] = "No field handles in row 2";
        } 

        if (empty($result['data'])) {
            $result['errors'][] = "No data rows found from row 3";
        }
        
        return $result;
    }


    /**
     * Import, or attempt to import the parsed data.
     * 
     * @param array $parsedData Parsed data from the spreadsheet
     * @param bool $dryrun True if it's a dry run with nothing saved
     * 
     */
    public function import(array $parsedData, bool $dryrun) 
    {
        try {
            // set stuff up
            $this->_author = Craft::$app->getUser()->getIdentity();

            // get the imported data
            $this->_siteHandle = $parsedData['site'];     

            // now we know what site this is for, do some basic Craft checks:
            $this->_site = Craft::$app->sites->getSiteByHandle($this->_siteHandle); 
            if (!$this->_site) {
                $this->_importErrors[] = "Can’t find site with handle " . $this->_siteHandle;
                return false;                
                // throw new \Exception("Can’t find site with handle " . $this->_siteHandle);
            }

            $this->_plotsSection = Craft::$app->sections->getSectionByHandle($this->_plotsSectionHandle);
            if ($this->_plotsSection) {
                // does this section have settings in the selected site?
                $siteSettings = $this->_plotsSection->getSiteSettings();
                if (!isset($siteSettings[$this->_site->id])) {
                    $this->_importErrors[] = "Section " . $this->_plotsSection->name . " is not enabled for site " . $this->_site->name;
                    return false;
                    // throw new \Exception("Section " . $this->_plotsSection->name . " is not enabled for site " . $this->_site->name);
                }

                // get and set the entry types in this section
                $this->setSectionEntryTypes();
                if (!isset($this->_sectionEntryTypes[$this->_plotEntryTypeHandle])) {
                    $this->_importErrors[] = "Entry type " . $this->_plotEntryTypeHandle . " not found in section " . $this->_plotsSection->name;
                    return false;
                    // throw new \Exception("Entry type " . $this->_plotEntryTypeHandle . " not found in section " . $this->_plotsSection->name);
                } 

                // validate the import field handles
                if ( ! $this->validateImportFieldHandles($parsedData) ) {
                    $this->_importErrors[] = "The import data contains invalid fields for the entry type";
                    return false;
                    // throw new \Exception("The import data contains invalid fields for the entry type");            
                }

                // we have apparently valid source data. 
                // stick it in yer pocket.

                $this->_header = $parsedData['handles'];
                $this->_data = $parsedData['data'];


                // we've got this far, attempt an import
                if (!$dryrun) {
                    // back her up, captain
                    $this->backupBeforeImport();

                    $result = $this->importPlots($dryrun);
                    return $result;
                } else {
                    return true;
                }

            } else {
                // bork. can't import if no section.
                $this->__importErrors[] = "There is no section with handle " . $this->_plotsSectionHandle;
                throw new \Exception("There is no section with handle " . $this->_plotsSectionHandle);
            }
        } catch (\Exception $e) {
            Craft::$app->session->setError($e->getMessage());
            return false;
        }

    }

    /**
     * @param bool $dryrun Perform a dry run if true
     * @return array Array with keys of 'imported' and 'failed'
     */
    private function importPlots(bool $dryrun)
    {
        Craft::info("Importing " . count($this->_data) . " plots.", __METHOD__);

        $status = true;
        $row = 1;
        foreach ($this->_data as $plotData) {
            if ($this->validatePlotDataRow($plotData, $row)) {
                Craft::info("Importing valid plot at row ". $row, __METHOD__);

                $plot = $this->importPlot($plotData, $dryrun);
                if ($plot) {
                    $this->_importedData[] = $plotData;
                    $status = $status && true;
                } else {
                    $this->_failedImportData[] = $plotData;
                    $status = $status && false;
                }
            } else {
                // something invalid in the plot data row
                $message = "Not importing invalid plot at row ". $row;
                Craft::info($message, __METHOD__);
                $this->_importErrors[] = $message;
                $this->_failedImportData[] = $plotData;
                $status = $status && false;
            }

            $row++;
        }
        Craft::info("All plot data processed for import", __METHOD__);
        return $status;
    }

    /**
     * Import a plot. Assumes $plotData has been validated!
     * 
     */
    private function importPlot(array $plotData, bool $dryrun)
    {
        // assumes the data is valid
        try {
            $entry = $this->createEntryModel($plotData);
            if ($entry) {
                Craft::info("Entry model created." , __METHOD__);

                // if not dry-running, save it too.
                if (!$dryrun) {
                    Craft::info('Saving new entry "'.$entry->title.'"' , __METHOD__);
                    // Craft::dd($entry);
                    $success = Craft::$app->elements->saveElement($entry, true);
                    if (!$success) {
                        $message = 'Couldn’t save the entry "'.$entry->title.'"';
                        $saveErrors = $entry->getErrorSummary(true);
                        // $this->_importErrors[] = $message;
                        $this->_importErrors[] = $entry->title . ": " .  implode(",",$saveErrors);
                        Craft::error($message, __METHOD__);
                        return false;
                    } else {
                        Craft::info('Entry saved: "'.$entry->title.'"', __METHOD__);
                        return $this->attachAssets($entry);
                    }
                } else {
                    return true;
                }
            } else {
                $message = "Entry model NOT created.";
                $this->_importErrors[] = $message;
                Craft::error("Entry model NOT created." , __METHOD__);
            }
        } catch (\Throwable $e) {
            $message = 'Couldn’t save the entry' . $e->getMessage();
            $this->_importErrors[] = $message;
            Craft::error($message, __METHOD__);
            return false;
        }
    }

    private function attachAssets(Entry $entry) {
        // don't think I'm doing this here, separate event handler created
        // so that it works for non-imported plots too

        return true;
    }

    /**
     * checks the field handles are valid for the selected Section/EntryType/FieldLayout
     * 
     * @return bool
     * 
     */
    private function validateImportFieldHandles($extractedData) 
    {
        $invalidHandles = [];
        if (!empty($extractedData['handles'])) {
            foreach ($extractedData['handles'] as $fieldHandle) {
                if (! in_array($fieldHandle, $this->_sectionEntryTypes[$this->_plotEntryTypeHandle]['fieldHandles'])
                    && ! in_array($fieldHandle, $this->_customSchema) ) {
                    $invalidHandles[] = $fieldHandle;
                }
            }
        }

        if (empty($invalidHandles)) {   
            return true;
        } else {
            $this->_importErrors = array_merge($this->_importErrors, $invalidHandles);
            return false;
        }
    }

    /**
     * Validate the custom fields
     */
    private function validatePlotDataRow($plotData, $rownum)
    {
        Craft::info("Start validating plot data at row " . $rownum, __METHOD__);
        $customFields = []; // gather these up, although probably unnecessary
        $rowErrors = [];

        try {
            // check required things aren't null, check things are valid parents
            // loop through the list of known/expected fields
            foreach ($this->_entryTypeFields as $expectedHandle => $fieldAttributes) {
                if (array_key_exists($expectedHandle, $plotData) && !empty($plotData[$expectedHandle]) ) {
                    $value = $plotData[$expectedHandle];
                    if ($fieldAttributes['type'] == 'string') {
                        // fine, as-is
                        $customFields[ $expectedHandle ] = $value;
                    } elseif ($fieldAttributes['type'] == 'entryId') {
                        // check the entry type matches what's expected
                        $entryId = (int) $value;
                        $relatedEntry = Entry::find()
                            ->id($entryId)
                            ->siteId($this->_site->id)
                            ->one();

                        if ($relatedEntry) {

                            if ( $relatedEntry->type->handle == $fieldAttributes['entryType'] ) {
                                $customFields[ $expectedHandle ] = $value;
                            } else {
                                $rowErrors[] = 
                                    "Plot: " . $plotData['title']
                                    . ". Unexpected entryType of " . $relatedEntry->type->handle 
                                    . " found for field " . $expectedHandle 
                                    . ", expecting " . $fieldAttributes['entryType'];
                            }

                        } else {
                            $rowErrors[] = 
                                "Plot: " . $plotData['title']
                                . ". Related entry with ID " . $entryId . " not found for field " . $expectedHandle 
                                . ", expecting " . $fieldAttributes['entryType'];
                        }
                        
                    } elseif ($fieldAttributes['type'] == 'list') {
                        // split the list by delimiter into an array
                        $customFields[ $expectedHandle ] = explode(',',$value);
                    } else {
                        $customFields[ $expectedHandle ] = $value;
                    }
                    

                } else {
                    if ($fieldAttributes['required']) {
                        // bork, this field is required
                        $rowErrors[] = "Plot: " . $plotData['title']
                            . ". Field " . $expectedHandle . " is required, but missing or empty";
                    }
                }
            }
        } catch (\Throwable $e) {
            Craft::error("Exception thrown at plot data row " . $rownum . ". Error count: " . count($rowErrors), __METHOD__);
            $this->_importErrors[] = $e->getMessage();
            $this->_importErrors[] = $rowErrors;
            return false;            
        }
        Craft::info("Finished validating plot data row " . $rownum . ". Error count: " . count($rowErrors), __METHOD__);
        if ($rowErrors) {
            $this->_importErrors[] = $rowErrors;
            return false;
        } else {
            return true;
        }
    }




    private function setSectionEntryTypes()
    {
        $plotEntryTypes = $this->_plotsSection->getEntryTypes();
        if ($plotEntryTypes) {
            foreach ($plotEntryTypes as $entryType) {
                $etFieldHandles = [];
                $fields = $entryType->getFieldLayout()->getFields();
                foreach ($fields as $field) {
                    $etFieldHandles[] = $field->handle;
                }
                $this->_sectionEntryTypes[$entryType->handle] = [
                    'handle' => $entryType->handle,
                    'id' => $entryType->id,
                    'fieldHandles' => $etFieldHandles
                ];
            }
        } else {
            // there should be at least one, unless it's a single?? bork
            $message = "No entry types found for " . $this->_plotsSection->name;
            Craft::error($message, __METHOD__);
            throw new \Exception($message);
        }
    }

    /**
     * Create a basic Entry
     * 
     */
    private function createEntryModel($plotData)
    {
        Craft::info("Creating entry model from plot data", __METHOD__);
        $fieldValues = [];

        try {
            $entry = new Entry();

            $entry->authorId = $this->_author->id;
            $entry->sectionId = $this->_plotsSection->id;
            $entry->typeId = $this->_sectionEntryTypes[$this->_plotEntryTypeHandle]['id'];

            // set title
            $entry->title = $plotData['title'];


            // $entry->slug = $data['slug'];
            $entry->postDate = new \DateTime();
            $entry->expiryDate = '';

            $entry->enabled = true;
            $entry->enabledForSite = true;

            $entry->siteId = $this->_site->id;


            // check fields that are parents. Building takes precedence over Phase

            // this field should have been validated as existing, but belt and braces
            if ( array_key_exists('phase', $plotData) ){
                $phaseId = $this->getParentId($plotData['phase'], $plotData['phase-slug']);
                if ($phaseId) {
                    $entry->newParentId = $phaseId;
                }
            }

            // The building ID is optional. If it's not there, create a new entry for it?
            if ( array_key_exists('building', $plotData) ) {
                $buildingId = $this->getParentId($plotData['building'], $plotData['building-slug']);
                if ($buildingId) {
                    $entry->newParentId = $buildingId;
                } else {
                    // if no building entry exists, stick it under the Phase instead.
                    $entry->newParentId = $phaseId;
                }
            }

            foreach ($this->_entryTypeFields as $handle => $options) {
                // if there's data in the field
                if ( $options['fv'] == true ) {
                    if ( array_key_exists($handle, $plotData) && $plotData[$handle] ) {
                        if ($options['type'] == 'entryId') {
                            $fieldValues[$handle] = [$plotData[$handle]];
                        } elseif ($options['type'] == 'supertable') {
                            $superTableData = [];
                            $stField = Craft::$app->getFields()->getFieldByHandle($handle);
                            if ($stField && $handle == 'pl_price') {
                                $blocktypes = Supertable::$plugin->getService()->getBlockTypesByFieldId($stField->id);
                                $blockType = $blocktypes[0]; // Only ever one

                                // price is the price
                                // date is today
                                $superTableData['new1'] = [
                                    'type' => $blockType->id,
                                    'enabled' => true,
                                    'fields' => [
                                        'price' => $plotData[$handle],
                                        'datePriceSet' => Carbon::now()
                                    ]
                                ];
                                                        
                            }
                            if ($superTableData) {
                                $fieldValues[$handle] = $superTableData;
                            }
                        } else {
                            $fieldValues[$handle] = $plotData[$handle];
                        }
                        
                    } else {
                        $message = "No data for handle " . $handle . " in plotData";
                        // $this->_importErrors[] = $message;
                        Craft::info($message, __METHOD__);
                    }
                }
                
            }

            $entry->setFieldValues($fieldValues);
            return $entry;
        } catch (\Throwable $e) {
            $message = "Failed to create entry model from plot data: " . $e->getMessage();
            $this->_importErrors[] = $message;
            Craft::error($message, __METHOD__);

            return null;
        }

    }

    private function getParentId($id, $slug)
    {
        $query = Entry::find();
        $query->sectionId($this->_plotsSection->id);
        $query->siteId($this->_site->id);
        $query->slug($slug);
        $query->status(null);
        $query->id($id);
        $parent = $query->one();
        if ($parent) {
            return $parent->id;
        } else {
            return null;
        }

    }

    public function getImportErrors()
    {
        return $this->_importErrors;
    }

    public function getImportMessage()
    {
        return $this->_importMessage;
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getHeader()
    {
        return $this->_header;
    }

    public function getSite()
    {
        return $this->_site;
    }

    public function getPlotsSection()
    {
        return $this->_plotsSection;
    }

    public function getFailedImportData()
    {
        return $this->_failedImportData;
    }

    public function getImportedData()
    {
        return $this->_importedData;
    }


    /** 
     * Make a backup of the database
     */
    private function backupBeforeImport()
    {
        $backupPath = Craft::$app->getPath()->getDbBackupPath();
        $file = $backupPath . '/homeselectorimporter-' . gmdate('ymd_His') . '.sql';
        Craft::$app->getDb()->backupTo($file);
    }    
}
