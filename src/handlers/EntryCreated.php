<?php
namespace pixelimage\homeselectorimporter\handlers;
use pixelimage\homeselectorimporter\HomeSelectorImporter;
use craft\events\ModelEvent;
use craft\helpers\ElementHelper;
use Craft;

class EntryCreated
{

    public function __invoke(ModelEvent $event)
    {
        $entry = $event->sender;
        $isNew = $event->firstSave;
        
        // If it's a new Entry, go see if we need to auto-attach any assets
        if ($isNew) {
            Craft::info("New plot, starting attacher", __METHOD__);
            HomeSelectorImporter::$plugin->importAssets->initAttach($entry);      
        } else {
            Craft::info("Not new for entry: " . $entry->title, __METHOD__);
        }
    }
}