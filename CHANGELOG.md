# HomeSelectorImporter Changelog

## 2.0.0 - 2023-11-09
### Changed
- Replace archived XLS library box/sprout with opensprout/opensprout. Requires PHP8.

## 1.2.7 - 2021-02-08
### Changed
- Improved error reporting for failed imports

## 1.2.6 - 2021-02-08
### Changed
- Require nesbot/carbon, just in case Craft decides to drop it

## 1.2.5 - 2021-02-08
### Changed
- Add Plot mask ref
- Changelog URL

## 1.2.3 - 2020-11-22
### Changed
- Homeselector structural changes

## 1.2.2 - 2020-11-03
### Changed
- Auto-attach assets to matrix fields now that Craft has been fixed (#7108)
- Craft minimum version is now 3.5.16

## 1.2.1 - 2020-11-03
### Changed
- Improve handling of no data uploaded

## 1.2 - 2020-11-03
### Added
- Move working data to dedicated model and out of project config

## 1.1 - 2020-11-02
### Added
- Auto-attach some assets
- Create proper settings page

## 1.0.0 - 2020-09-20
### Added
- Initial release
