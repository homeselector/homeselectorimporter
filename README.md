# HomeSelectorImporter plugin for Craft CMS 3.x

Imports Home Selector entries and assets

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.5.16 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require pixelimage/home-selector-importer

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for HomeSelectorImporter.

## HomeSelectorImporter Overview

-Insert text here-

## Configuring HomeSelectorImporter

-Insert text here-

## Using HomeSelectorImporter

-Insert text here-

## HomeSelectorImporter Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Andrew Wallace](https://www.prettyabstract.co.uk/)
